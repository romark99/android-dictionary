package com.brest.romark.mydictionary.viewmodel;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.brest.romark.mydictionary.entity.Word;
import com.brest.romark.mydictionary.repository.WordRepository;

import java.util.List;

public class WordViewModel extends AndroidViewModel {

    private WordRepository wordRepository;

    private LiveData<List<Word>> words;

    public WordViewModel (Application application) {
        super(application);
        wordRepository = new WordRepository();
        words = wordRepository.getAllWords();
    }

    public LiveData<List<Word>> getAllWords() { return words; }

    public void insert(Word word) { wordRepository.insert(word); }
}

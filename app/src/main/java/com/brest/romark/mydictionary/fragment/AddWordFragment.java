package com.brest.romark.mydictionary.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.brest.romark.mydictionary.R;
import com.brest.romark.mydictionary.entity.Word;
import com.brest.romark.mydictionary.viewmodel.WordViewModel;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;

import java.util.List;


public class AddWordFragment extends Fragment {

    private OnAddWordListener addWordListener;

    private TextInputEditText tietWord;

    private TextInputEditText tietLangId;

    private WordViewModel wordViewModel;

    private MaterialButton btnAdd;

    public AddWordFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_add_word, container, false);
        tietWord = view.findViewById(R.id.tietWord1);
        tietLangId = view.findViewById(R.id.tietLangId);
        btnAdd = view.findViewById(R.id.btnAdd);

        wordViewModel = ViewModelProviders.of(this).get(WordViewModel.class);
//        wordViewModel.getAllWords().observe(this, new Observer<List<Word>>() {
//            @Override
//            public void onChanged(List<Word> words) {
//                adapter.setWords(words);
//            }
//        });
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Word word = new Word();
                word.setWord(tietWord.getText().toString());
                word.setLang_id(Long.parseLong(tietLangId.getText().toString()));
                wordViewModel.insert(word);
                if (addWordListener != null) {
                    addWordListener.onAddWord();
                }
            }
        });
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnAddWordListener) {
            addWordListener = (OnAddWordListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnWordListFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        addWordListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnAddWordListener {
        void onAddWord();
    }
}

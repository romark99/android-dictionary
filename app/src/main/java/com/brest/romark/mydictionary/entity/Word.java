package com.brest.romark.mydictionary.entity;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.util.Objects;


@Entity(foreignKeys = @ForeignKey(
        entity = Language.class,
        parentColumns = "lang_id",
        childColumns = "lang_id"),
        indices = @Index("lang_id"))
public class Word {

    @PrimaryKey(autoGenerate = true)
    private long word_id;

    @NonNull
    private long lang_id;

    @NonNull
    private String word;

    public long getWord_id() {
        return word_id;
    }

    public void setWord_id(long word_id) {
        this.word_id = word_id;
    }

    public long getLang_id() {
        return lang_id;
    }

    public void setLang_id(long lang_id) {
        this.lang_id = lang_id;
    }

    @NonNull
    public String getWord() {
        return word;
    }

    public void setWord(@NonNull String word) {
        this.word = word;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Word word1 = (Word) o;
        return lang_id == word1.lang_id &&
                word.equals(word1.word);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lang_id, word);
    }
}

package com.brest.romark.mydictionary.repository;

import android.os.AsyncTask;
import android.widget.Toast;

import androidx.lifecycle.LiveData;

import com.brest.romark.mydictionary.App;
import com.brest.romark.mydictionary.dao.WordDao;
import com.brest.romark.mydictionary.database.DictionaryDatabase;
import com.brest.romark.mydictionary.entity.Word;

import java.util.List;

public class WordRepository {

    private WordDao wordDao;

    private LiveData<List<Word>> words;

    public WordRepository() {
        DictionaryDatabase db = App.getInstance().getDatabase();
        this.wordDao = db.wordDao();
        this.words = wordDao.getAllWords();
    }

    public LiveData<List<Word>> getAllWords() {
        return words;
    }

    public void insert(Word word) {
        new InsertAsyncTask(wordDao).execute(word);
    }

    private static class InsertAsyncTask extends AsyncTask<Word, Void, Long> {

        private WordDao asyncTaskDao;

        InsertAsyncTask(WordDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Long doInBackground(final Word... params) {
            return asyncTaskDao.insert(params[0]);
        }

        @Override
        protected void onPostExecute(Long id) {
            super.onPostExecute(id);
            Toast.makeText(App.getAppContext(), id != null ?
                    "Added word's id: " + id :
                    "The word was NOT added.", Toast.LENGTH_SHORT).show();
        }
    }
}

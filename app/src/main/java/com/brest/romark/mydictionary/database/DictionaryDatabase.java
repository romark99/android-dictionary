package com.brest.romark.mydictionary.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.brest.romark.mydictionary.dao.LanguageDao;
import com.brest.romark.mydictionary.dao.WordDao;
import com.brest.romark.mydictionary.entity.Definition;
import com.brest.romark.mydictionary.entity.Language;
import com.brest.romark.mydictionary.entity.Word;
import com.brest.romark.mydictionary.entity.WordRelation;

@Database(entities = {
        Word.class,
        Definition.class,
        WordRelation.class,
        Language.class
}, version = 1)
public abstract class DictionaryDatabase extends RoomDatabase {

    public abstract LanguageDao languageDao();
    public abstract WordDao wordDao();
}

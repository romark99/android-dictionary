package com.brest.romark.mydictionary.entity;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;

@Entity(foreignKeys = {
        @ForeignKey(
                entity = Word.class,
                parentColumns = "word_id",
                childColumns = "word1_id"
        ),
        @ForeignKey(
                entity = Word.class,
                parentColumns = "word_id",
                childColumns = "word2_id"
        )
},
        primaryKeys = {"word1_id", "word2_id"},
        indices = @Index("word2_id"))
public class WordRelation {

    @NonNull
    private long word1_id;

    @NonNull
    private long word2_id;

    private String example1;

    private String example2;

    public long getWord1_id() {
        return word1_id;
    }

    public void setWord1_id(long word1_id) {
        this.word1_id = word1_id;
    }

    public long getWord2_id() {
        return word2_id;
    }

    public void setWord2_id(long word2_id) {
        this.word2_id = word2_id;
    }

    public String getExample1() {
        return example1;
    }

    public void setExample1(String example1) {
        this.example1 = example1;
    }

    public String getExample2() {
        return example2;
    }

    public void setExample2(String example2) {
        this.example2 = example2;
    }
}

package com.brest.romark.mydictionary.diffutil.callback;

import androidx.recyclerview.widget.DiffUtil;

import com.brest.romark.mydictionary.entity.Word;

import java.util.ArrayList;
import java.util.List;

public class WordDiffUtilCallback extends DiffUtil.Callback {

    private final List<Word> oldList;

    private final List<Word> newList;

    public WordDiffUtilCallback(List<Word> oldList, List<Word> newList) {
        this.oldList = oldList != null ? oldList : new ArrayList<Word>();
        this.newList = newList != null ? newList : new ArrayList<Word>();
    }

    @Override
    public int getOldListSize() {
        return oldList.size();
    }

    @Override
    public int getNewListSize() {
        return newList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        Word oldWord = oldList.get(oldItemPosition);
        Word newWord = newList.get(newItemPosition);
        return oldWord.getWord_id() == newWord.getWord_id();
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        Word oldWord = oldList.get(oldItemPosition);
        Word newWord = newList.get(newItemPosition);
        return oldWord.equals(newWord);
    }
}

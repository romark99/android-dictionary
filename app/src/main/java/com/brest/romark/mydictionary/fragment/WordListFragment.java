package com.brest.romark.mydictionary.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.brest.romark.mydictionary.App;
import com.brest.romark.mydictionary.MainActivity;
import com.brest.romark.mydictionary.R;
import com.brest.romark.mydictionary.adapter.WordListAdapter;
import com.brest.romark.mydictionary.diffutil.callback.WordDiffUtilCallback;
import com.brest.romark.mydictionary.entity.Word;
import com.brest.romark.mydictionary.viewmodel.WordViewModel;

import java.util.ArrayList;
import java.util.List;


public class WordListFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private OnWordListFragmentListener mListener;

    private WordListAdapter adapter;

    private WordViewModel wordViewModel;

    public WordListFragment() {
        // Required empty public constructor
    }

    public static WordListFragment newInstance(String param1, String param2) {
        WordListFragment fragment = new WordListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        wordViewModel = ViewModelProviders.of(this).get(WordViewModel.class);
        wordViewModel.getAllWords().observe(this, new Observer<List<Word>>() {
            @Override
            public void onChanged(List<Word> words) {
                WordDiffUtilCallback wordDiffUtilCallback = new WordDiffUtilCallback(adapter.getWords(), words);
                DiffUtil.DiffResult wordDiffResult = DiffUtil.calculateDiff(wordDiffUtilCallback);

                adapter.setWords(words);
                wordDiffResult.dispatchUpdatesTo(adapter);
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_word_list, container, false);
        RecyclerView rvWords = view.findViewById(R.id.rvWord);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());

//        layoutManager.setOrientation(RecyclerView.VERTICAL);
        rvWords.setLayoutManager(layoutManager);

        List<Word> words = wordViewModel.getAllWords().getValue();

        adapter = new WordListAdapter(App.getAppContext(), words);

        rvWords.setAdapter(adapter);
        rvWords.setItemAnimator(new DefaultItemAnimator());


        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnWordListFragmentListener) {
            mListener = (OnWordListFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnWordListFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnWordListFragmentListener {
        void onWordList(Uri uri);
    }
}

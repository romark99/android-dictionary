package com.brest.romark.mydictionary.entity;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Language {

    @PrimaryKey(autoGenerate = true)
    private long lang_id;

    @NonNull
    private String name;

    @NonNull
    private String short_name;

    public long getLang_id() {
        return lang_id;
    }

    public void setLang_id(long lang_id) {
        this.lang_id = lang_id;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    @NonNull
    public String getShort_name() {
        return short_name;
    }

    public void setShort_name(@NonNull String short_name) {
        this.short_name = short_name;
    }
}

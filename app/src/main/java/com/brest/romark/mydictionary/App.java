package com.brest.romark.mydictionary;

import android.app.Application;
import android.content.Context;

import androidx.room.Room;

import com.brest.romark.mydictionary.database.DictionaryDatabase;

public class App extends Application {

    public static App instance;

    private static Context context;

    private DictionaryDatabase database;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        App.context = getApplicationContext();
        database = Room.databaseBuilder(this, DictionaryDatabase.class, "dictionary_db")
//                .allowMainThreadQueries()
                .build();
    }

    public static App getInstance() {
        return instance;
    }

    public DictionaryDatabase getDatabase() {
        return database;
    }

    public static Context getAppContext() {
        return App.context;
    }
}

package com.brest.romark.mydictionary.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import androidx.databinding.DataBindingUtil;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.brest.romark.mydictionary.R;
import com.brest.romark.mydictionary.databinding.WordListItemBinding;
import com.brest.romark.mydictionary.entity.Word;

//public class WordListAdapter extends PagedListAdapter<Word, WordListAdapter.WordViewHolder> {

public class WordListAdapter extends RecyclerView.Adapter<WordListAdapter.WordViewHolder> {

    private final LayoutInflater inflater;

    private List<Word> words;

    public WordListAdapter(Context context, List<Word> words) {
        inflater = LayoutInflater.from(context);
        this.words = words;
    }

//    public WordListAdapter(Context context, DiffUtil.ItemCallback<Word> diffUtilCallback) {
//        super(diffUtilCallback);
//        inflater = LayoutInflater.from(context);
//    }

    @NonNull
    @Override
    public WordListAdapter.WordViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        WordListItemBinding binding = DataBindingUtil.inflate(inflater, R.layout.word_list_item, parent, false);
        return new WordViewHolder(binding);
//        View wordView = inflater.inflate(R.layout.word_list_item, parent, false);
//        return new WordViewHolder(wordView);
    }

//    @NonNull
//    @Override
//    public WordListAdapter.WordViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        View wordView = inflater.inflate(R.layout.word_list_item, parent, false);
//        return new WordViewHolder(wordView);
//    }

    @Override
    public void onBindViewHolder(@NonNull WordViewHolder holder, int position) {
//        if (words != null) {
//            Word current = words.get(position);
//            holder.wordItemView.setText(current.getWord());
//        } else {
//            holder.wordItemView.setText("No word");
//        }
        if (words != null) {
            holder.bind(words.get(position));
        }
    }

//    @Override
//    public void onBindViewHolder(@NonNull WordViewHolder holder, int position) {
//        holder.bind(getItem(position));
//        if (words != null) {
//            Word current = words.get(position);
//            holder.wordItemView.setText(current.getWord());
//        } else {
//            holder.wordItemView.setText("No word");
//        }
//    }

    class WordViewHolder extends RecyclerView.ViewHolder {

        WordListItemBinding binding;

        public WordViewHolder(WordListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(Word word) {
            binding.setWord(word);
            binding.executePendingBindings();
        }
    }

    @Override
    public int getItemCount() {
        if (words != null) {
            return words.size();
        }
        else return 0;
    }

    public void setWords(List<Word> words) {
        this.words = words;
    }

    public List<Word> getWords() {
        return words;
    }
}

package com.brest.romark.mydictionary.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.brest.romark.mydictionary.entity.Language;

import java.util.List;

@Dao
public interface LanguageDao {

    @Query("SELECT * FROM language")
    List<Language> getAll();

    @Query("SELECT lang_id FROM language WHERE short_name = :shortName")
    long getIdByShortName(String shortName);

    @Insert
    void insert(Language language);
}

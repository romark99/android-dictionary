package com.brest.romark.mydictionary.adapter.binding;

import android.widget.TextView;

import androidx.databinding.BindingAdapter;

public class BindingAdapters {

    @BindingAdapter({"android:text"})
    public static void setLongText(TextView textView, long id) {
        textView.setText(String.valueOf(id));
    }
}

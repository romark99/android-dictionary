package com.brest.romark.mydictionary.entity;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

@Entity(
        foreignKeys = @ForeignKey(
                entity = Word.class,
                parentColumns = "word_id",
                childColumns = "word_id"),
        primaryKeys = {"word_id", "num_of_def1", "num_of_def2"}
)
public class Definition {

    @NonNull
    private long word_id;

    private int num_of_def1;

    private int num_of_def2;

    @NonNull
    private String definition;

    private String example;

    public long getWord_id() {
        return word_id;
    }

    public void setWord_id(long word_id) {
        this.word_id = word_id;
    }

    public int getNum_of_def1() {
        return num_of_def1;
    }

    public void setNum_of_def1(int num_of_def1) {
        this.num_of_def1 = num_of_def1;
    }

    public int getNum_of_def2() {
        return num_of_def2;
    }

    public void setNum_of_def2(int num_of_def2) {
        this.num_of_def2 = num_of_def2;
    }

    @NonNull
    public String getDefinition() {
        return definition;
    }

    public void setDefinition(@NonNull String definition) {
        this.definition = definition;
    }

    public String getExample() {
        return example;
    }

    public void setExample(String example) {
        this.example = example;
    }
}

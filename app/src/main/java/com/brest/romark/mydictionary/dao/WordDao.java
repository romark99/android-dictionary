package com.brest.romark.mydictionary.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.brest.romark.mydictionary.entity.Word;

import java.util.List;

@Dao
public interface WordDao {
    @Query("SELECT * FROM word")
    LiveData<List<Word>> getAllWords();

    @Insert
    Long insert(Word word);

    @Query("DELETE FROM word")
    void deleteAll();

}
